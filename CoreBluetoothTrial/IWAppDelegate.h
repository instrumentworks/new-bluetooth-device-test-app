//
//  IWAppDelegate.h
//  CoreBluetoothTrial
//
//  Created by Shane Cox on 15/08/13.
//  Copyright (c) 2013 Shane Cox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IWAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

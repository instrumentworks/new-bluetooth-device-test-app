//
//  main.m
//  CoreBluetoothTrial
//
//  Created by Shane Cox on 15/08/13.
//  Copyright (c) 2013 Shane Cox. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "IWAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([IWAppDelegate class]));
    }
}

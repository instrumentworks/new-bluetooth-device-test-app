//
//  CoreBluetoothTrialTests.m
//  CoreBluetoothTrialTests
//
//  Created by Shane Cox on 15/08/13.
//  Copyright (c) 2013 Shane Cox. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface CoreBluetoothTrialTests : XCTestCase

@end

@implementation CoreBluetoothTrialTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
